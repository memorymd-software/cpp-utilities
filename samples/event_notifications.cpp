#include "event_notifier.h"
#include <iostream>
#include <string>
#include <mutex>

using Neuro::Utilities::EventNotifier;
using Neuro::Utilities::EventListener;

class SampleClass final
{
	int mValue{0};
public:
	EventNotifier<std::string, int> ValueChanged;

	void setValue(int value)
	{
		mValue = value;
		ValueChanged.notify("Block", mValue);
	}

	void setValueAsync(int value)
	{
		mValue = value;
		ValueChanged.notifyAsync("Async", mValue);
	}
};

class SampleClass2 final
{
	int mValue{0};
public:
	EventNotifier<> ValueChanged;

	void setValue(int value)
	{
		mValue = value;
		ValueChanged.notify();
	}

	void setValueAsync(int value)
	{
		mValue = value;
		ValueChanged.notifyAsync();
	}
};

int main()
{
	SampleClass sampleObj1;
	SampleClass sampleObj2;
	SampleClass sampleObj3;
	SampleClass sampleObj4;
	SampleClass sampleObj5;
	SampleClass2 sampleObj6;
	std::mutex outputMutex;

	const EventListener<std::string, int> listener1
	{
		[&outputMutex](std::string message, int value)
		{
			std::unique_lock outputLock(outputMutex);
			std::cout << "Message1: " << message;
			std::cout << " Value1: " << value << std::endl;
			std::cout << "-------------------" << std::endl;
		}
	};
	const EventListener<std::string, int> listener2
	{
		[&outputMutex](const std::string &message, int value)
		{
			std::unique_lock outputLock(outputMutex);
			std::cout << "Message2: " << message;
			std::cout << " Value2: " << value << std::endl;
			std::cout << "-------------------" << std::endl;
		}
	};
	const EventListener<std::string, int> listener3
	{
		[&outputMutex](const std::string &message, int value)
		{
			std::unique_lock outputLock(outputMutex);
			std::cout << "Message3: " << message;
			std::cout << " Value3: " << value << std::endl;
			std::cout << "-------------------" << std::endl;
		}
	};
	const EventListener<std::string, int> listener4
	{
		[&outputMutex](const std::string &message, int value)
		{
			std::unique_lock outputLock(outputMutex);
			std::cout << "Message4: " << message;
			std::cout << " Value4: " << value << std::endl;
			std::cout << "-------------------" << std::endl;
		}
	};
	const EventListener<std::string, int> listener5
	{
		[&outputMutex](const std::string &message, int value)
		{
			std::unique_lock outputLock(outputMutex);
			std::cout << "Message5: " << message;
			std::cout << " Value5: " << value << std::endl;
			std::cout << "-------------------" << std::endl;
		}
	};
	const EventListener<> listener6
	{
		[&outputMutex]()
		{
			std::unique_lock outputLock(outputMutex);
			std::cout << "Message6: " << "Nothing";
			std::cout << " Value6: " << "Nothing" << std::endl;
			std::cout << "-------------------" << std::endl;
		}
	};
	sampleObj1.ValueChanged.registerListener(listener1);
	sampleObj2.ValueChanged.registerListener(listener2);
	sampleObj3.ValueChanged.registerListener(listener3);
	sampleObj4.ValueChanged.registerListener(listener4);
	sampleObj5.ValueChanged.registerListener(listener5);
	sampleObj6.ValueChanged.registerListener(listener6);

	int inputValue;
	while (std::cin >> inputValue)
	{
		auto thread1 = std::thread([&]() {
			for (auto i = 0; i < 500; ++i)
			{
				sampleObj1.setValueAsync(inputValue+i);
				sampleObj1.setValue(inputValue + i);
				sampleObj2.setValueAsync(inputValue + i);
				sampleObj2.setValue(inputValue + i);
				sampleObj3.setValueAsync(inputValue + i);
				sampleObj3.setValue(inputValue + i);
				sampleObj4.setValueAsync(inputValue + i);
				sampleObj4.setValue(inputValue + i);
				sampleObj5.setValueAsync(inputValue + i);
				sampleObj5.setValue(inputValue + i);
				sampleObj6.setValueAsync(0);
				sampleObj6.setValue(0);
			}
		});
		auto thread2 = std::thread([&]() {
			for (auto i = 0; i < 500; ++i)
			{
				sampleObj1.setValueAsync(inputValue + i);
				sampleObj1.setValue(inputValue + i);
				sampleObj2.setValueAsync(inputValue + i);
				sampleObj2.setValue(inputValue + i);
				sampleObj3.setValueAsync(inputValue + i);
				sampleObj3.setValue(inputValue + i);
				sampleObj4.setValueAsync(inputValue + i);
				sampleObj4.setValue(inputValue + i);
				sampleObj5.setValueAsync(inputValue + i);
				sampleObj5.setValue(inputValue + i);
				sampleObj6.setValueAsync(0);
				sampleObj6.setValue(0);
			}
		});
		auto thread3 = std::thread([&]() {
			for (auto i = 0; i < 500; ++i)
			{
				sampleObj1.setValueAsync(inputValue + i);
				sampleObj1.setValue(inputValue + i);
				sampleObj2.setValueAsync(inputValue + i);
				sampleObj2.setValue(inputValue + i);
				sampleObj3.setValueAsync(inputValue + i);
				sampleObj3.setValue(inputValue + i);
				sampleObj4.setValueAsync(inputValue + i);
				sampleObj4.setValue(inputValue + i);
				sampleObj5.setValueAsync(inputValue + i);
				sampleObj5.setValue(inputValue + i);
				sampleObj6.setValueAsync(0);
				sampleObj6.setValue(0);
			}
		});
		thread1.join();
		thread2.join();
		thread3.join();
	}
}
