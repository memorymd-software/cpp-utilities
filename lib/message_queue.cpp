#include "message_queue.h"
#include "thread_pool.h"


namespace Neuro::Utilities
{
	MessageQueue& MessageQueue::instance()
	{
		static MessageQueue messageQueue;
		return messageQueue;
	}
	
	bool MessageQueue::removeExpiredHasMessages(std::list<QueueListElement>& listen_queues)
	{
		using EraseIterator = typename std::remove_reference_t<decltype(listen_queues)>::iterator;

		auto hasMessages{ false };
		std::vector<EraseIterator> iteratorsToErase;
		for (auto listenerIterator = listen_queues.begin(); listenerIterator != listen_queues.end(); ++listenerIterator)
		{
			if (listenerIterator->Queue->expired())
			{
				iteratorsToErase.push_back(listenerIterator);
			}
			else if (!listenerIterator->Queue->empty())
			{
				hasMessages = true;
			}
		}

		for (auto iteratorToErase : iteratorsToErase)
		{
			try
			{
				if (iteratorToErase->FinishedFuture != nullptr && iteratorToErase->FinishedFuture->valid())
				{
					if (iteratorToErase->FinishedFuture->wait_for(std::chrono::microseconds(10)) != std::future_status::ready)
					{
						continue;
					}
				}								
				iteratorToErase->FinishedFuture.reset();
				iteratorToErase->RevokedPromise.set_value();				
			}
			catch (...)	{}
			
			listen_queues.erase(iteratorToErase);
		}

		return hasMessages;
	}
	
	MessageQueue::MessageQueue() :
		mDone(false),
		mFinishedFuture(mFinishedPromise.get_future()),
		mQueueThread([=]() {notificationThread(); })
	{}

	MessageQueue::~MessageQueue()
	{
		try
		{
			mDone = true;
			std::unique_lock revokeLock{ mRevokeMutex };
			mRevokeCondition.notify_one();
			revokeLock.unlock();
			int attempts = 100;
			while (--attempts > 0 && mFinishedFuture.valid() && mFinishedFuture.wait_for(std::chrono::microseconds(10)) != std::future_status::ready)
			{
				mRevokeCondition.notify_one();
			}
			mQueueThread.detach();
		}
		catch (...) {}
	}
	
	void MessageQueue::notificationThread()
	{
		while (!mDone)
		{
			std::unique_lock queueLock{ mRevokeMutex };
			if (!removeExpiredHasMessages(mListenerCollection))
			{
				mRevokeCondition.wait(queueLock);
				if (mDone) break;
				if (!removeExpiredHasMessages(mListenerCollection))
				{
					continue;
				}
			}
			queueLock.unlock();

			for (auto& elem : mListenerCollection)
			{
				if (mDone) break;
				if (elem.FinishedFuture != nullptr && elem.FinishedFuture->valid())
				{
					if (elem.FinishedFuture->wait_for(std::chrono::microseconds(1)) != std::future_status::ready)
					{
						continue;
					}
				}
				elem.FinishedFuture.reset();

				if (mDone) break;
				std::function<void()> task = [&elem]()
				{
					if (elem.Queue->expired())
					{
						return;
					}
					elem.Queue->send();
				};

				if (mDone) break;
				elem.FinishedFuture = std::make_unique<std::future<void>>(ThreadPool::taskRun(task));
			}
		}
		mFinishedPromise.set_value();
	}
}
