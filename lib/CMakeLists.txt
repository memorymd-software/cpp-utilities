cmake_policy(SET CMP0063 NEW)
set(CMAKE_CXX_VISIBILITY_PRESET hidden)
set(CMAKE_VISIBILITY_INLINES_HIDDEN YES)

add_library(utilities STATIC
		message_queue.cpp
		task_queue.cpp
		thread_pool.cpp
	)

set_target_properties(utilities PROPERTIES VERSION 0.1.0)
target_include_directories(utilities PUBLIC include)

# Setting C++17 clang flag explicitly if we're building for android
# This is temporary workaround for ndk bug https://github.com/android-ndk/ndk/issues/1049
if (ANDROID)
	target_compile_options(utilities PRIVATE -std=c++17)
endif(ANDROID)