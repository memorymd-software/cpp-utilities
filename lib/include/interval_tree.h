#ifndef INTERVAL_TREE_H
#define INTERVAL_TREE_H
#include <utility>
#include <map>
#include <algorithm>
#include <set>

namespace Neuro::Utilities
{
	struct Interval final
	{
		size_t Low;
		size_t High;
	};

	inline bool operator<(const Interval& lhs, const Interval& rhs)
	{
		return lhs.Low < rhs.Low;
	}

	template<typename T>
	struct IntervalExt final
	{
		Interval Inter;
		T Val;
	};


	template <typename T>
	bool operator<(const IntervalExt<T>& lhs, const IntervalExt<T>& rhs)
	{
		if (lhs.Inter < rhs.Inter) return true;
		if (rhs.Inter < lhs.Inter) return false;
		if (lhs.Val < rhs.Val) return true;
		return false;
	}
	
	template <typename T>
	bool intersects(const IntervalExt<T>& lhs, const IntervalExt<T>& rhs)
	{
		return lhs.Inter.Low <= rhs.Inter.High && rhs.Inter.Low <= lhs.Inter.High && (!(lhs.Val < rhs.Val) && !(rhs.Val < lhs.Val));
	}
	
	template <typename T>
	class IntervalTree final
	{		
		std::set<IntervalExt<T>> mSet;		
	public:		
		using key_type = Interval;
		using mapped_type = T;
		using value_type = std::pair<const key_type, mapped_type>;
		using size_type = size_t;
		using difference_type = std::ptrdiff_t;
		using iterator = typename decltype(mSet)::iterator;
		using const_iterator = typename decltype(mSet)::const_iterator;
		
		iterator begin() noexcept
		{
			return mSet.begin();
		}

		const_iterator begin() const
		{
			return mSet.begin();
		}
		
		const_iterator cbegin() const noexcept
		{
			return mSet.cbegin();
		}

		iterator end() noexcept
		{
			return mSet.end();
		}

		const_iterator end() const
		{
			return mSet.end();
		}

		const_iterator cend() const noexcept
		{
			return mSet.cend();
		}
		
		iterator rbegin() noexcept
		{
			return mSet.rbegin();
		}

		const_iterator rbegin() const
		{
			return mSet.rbegin();
		}
		
		const_iterator crbegin() const noexcept
		{
			return mSet.crbegin();
		}

		iterator rend() noexcept
		{
			return mSet.rend();
		}

		const_iterator rend() const
		{
			return mSet.rend();
		}

		const_iterator crend() const noexcept
		{
			return mSet.crend();
		}

		bool empty() const noexcept
		{
			return mSet.empty();
		}

		size_type size() const noexcept
		{
			return mSet.size();
		}

		void clear() noexcept
		{
			mSet.clear();
		}

		std::pair<iterator, iterator> find(const key_type& key, const T& val)
		{
			IntervalExt<T> ext
			{
				key,
				val
			};
			if (mSet.empty())
			{
				return { mSet.end(), mSet.end() };
			}
			auto nextItem = mSet.lower_bound(ext);
			if (nextItem == mSet.end())
			{
				nextItem = std::prev(nextItem);
			}
			if (nextItem == mSet.end())
			{
				return { mSet.end(), mSet.end() };
			}

			if (nextItem!=mSet.begin() && intersects(*std::prev(nextItem), ext))
			{
				--nextItem;
			}
			
			std::pair resultPair{ mSet.end(), mSet.end() };
			if (intersects(*nextItem, ext))
			{
				resultPair.first = nextItem;
				resultPair.second = ++nextItem;
				while(nextItem != mSet.end() && intersects(*nextItem, ext))
				{
					++nextItem;
					resultPair.second = nextItem;
				}
			}

			return resultPair;
		}
		
		std::pair<const_iterator, const_iterator> find(const key_type& key, const T& val) const
		{
			IntervalExt<T> ext
			{
				key,
				val
			};
			if (mSet.empty())
			{
				return { mSet.end(), mSet.end() };
			}
			auto nextItem = mSet.lower_bound(ext);
			if (nextItem == mSet.end())
			{
				nextItem = std::prev(nextItem);
			}
			if (nextItem == mSet.end())
			{
				return { mSet.end(), mSet.end() };
			}

			if (nextItem != mSet.begin() && intersects(*std::prev(nextItem), ext))
			{
				--nextItem;
			}

			std::pair resultPair{ mSet.end(), mSet.end() };
			if (intersects(*nextItem, ext))
			{
				resultPair.first = nextItem;
				resultPair.second = ++nextItem;
				while (nextItem != mSet.end() && intersects(*nextItem, ext))
				{
					++nextItem;
					resultPair.second = nextItem;
				}
			}

			return resultPair;
		}

		template <class M>
		std::pair<iterator, bool> insert_or_assign(const key_type& k, M&& obj)
		{			
			auto assigned = false;
			key_type newKey = k;
			auto same = find(newKey, obj);
			while (same.first != same.second)
			{
				assigned = true;
				newKey.Low = std::min(newKey.Low, same.first->Inter.Low);
				newKey.High = std::max(newKey.High, same.first->Inter.High);
				mSet.erase(same.first);
				same = find(newKey, obj);
			}
			auto [insertedIterator, _] = mSet.insert({ newKey, std::forward<M>(obj) });
			return { insertedIterator, assigned };
		}
	};

}
#endif // INTERVAL_TREE_H
