#ifndef OHM_H
#define OHM_H

#include <utility>

namespace Neuro::Utilities {

	template <typename RepresentationField>
	struct Ohm final {
		explicit constexpr Ohm() :mValue(RepresentationField{ 0 }) {}
		explicit constexpr Ohm(const RepresentationField& value) : mValue(value) {}
		explicit constexpr Ohm(RepresentationField&& value) : mValue(std::move(value)) {}

		constexpr const RepresentationField& value() const noexcept {
			return mValue;
		}

		constexpr RepresentationField& value() noexcept {
			return mValue;
		}

		constexpr RepresentationField kilo() const noexcept {
			return mValue / RepresentationField{ 1'000 };
		}

		constexpr RepresentationField mega() const noexcept {
			return mValue / RepresentationField{ 1'000'000 };
		}

	private:
		RepresentationField mValue;
	};

}
#endif // OHM_H
