#ifndef EVENT_LISTENER_H
#define EVENT_LISTENER_H

#include <functional>
#include <memory>

#include "message_queue.h"
#include "thread_pool.h"

namespace Neuro::Utilities
{
	template <typename... Args>
	class EventNotifier;
	
	template <typename... Args>
	class EventListener final
	{
	public:
		explicit EventListener(const std::function<void(Args ...)>& listener_function) :
			mImpl(std::make_unique<Impl>(listener_function))
		{}

		EventListener(const EventListener&) = delete;
		EventListener& operator=(const EventListener&) = delete;
		EventListener(EventListener&&) = default;
		EventListener& operator=(EventListener&&) = default;

		~EventListener() = default;

	private:
		using NotificationQueue = ReceiverQueue<Args...>;

		friend class EventNotifier<Args...>;

		[[nodiscard]] NotificationQueue queue() const noexcept
		{
			return mImpl->queue();
		}
		
		struct Impl final
		{		
			explicit Impl(const std::function<void(Args...)>& callback) :
				mMessageReceiver(callback),
				mReceiverQueue(MessageQueue::registerReceiver(mMessageReceiver))
			{}
			
			Impl(const Impl& other) = delete;
			Impl(Impl&& other) noexcept = delete;
			Impl& operator=(const Impl& other) = delete;
			Impl& operator=(Impl&& other) noexcept = delete;

			~Impl() = default;

			[[nodiscard]]
			NotificationQueue queue() const noexcept
			{
				return mReceiverQueue;
			}
			
		private:				
			MessageReceiver<Args...> mMessageReceiver;
			ReceiverQueue<Args...> mReceiverQueue;
		};

		std::unique_ptr<Impl> mImpl;
	};

}

#endif //EVENT_LISTENER_H