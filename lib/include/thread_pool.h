#ifndef THREAD_POOL_H
#define THREAD_POOL_H
#include <future>
#include <queue>

#include "thread_safe_queue.h"

namespace Neuro::Utilities
{	
	class ThreadPool final
	{
	public:
		ThreadPool(const ThreadPool& other) = delete;
		ThreadPool(ThreadPool&& other) noexcept = delete;
		ThreadPool& operator=(const ThreadPool& other) = delete;
		ThreadPool& operator=(ThreadPool&& other) noexcept = delete;
		~ThreadPool();
		
		template<typename R, typename... Args>
		static std::future<R> taskRun(const std::function<R(Args...)>& task, Args... args)
		{
			return instance().run(task, std::move(args)...);
		}
		
	private:
		class FunctionWrapper final
		{
			struct Impl
			{
				virtual ~Impl() = default;
				virtual void call() = 0;
			};

			std::unique_ptr<Impl> mImpl;

			template <typename F>
			struct ImplType : Impl
			{
				F mFunc;
				ImplType(F&& f):mFunc(std::move(f)){}
				void call() override { return mFunc(); };
			};

		public:
			template <typename F>
			explicit FunctionWrapper(F&& f, std::nullptr_t):
				mImpl(new ImplType<F>(std::forward<F>(f)))
			{}

			FunctionWrapper() = default;
			
			FunctionWrapper(const FunctionWrapper& other) = delete;
			FunctionWrapper(FunctionWrapper&& other) noexcept = default;
			FunctionWrapper& operator=(const FunctionWrapper & other) = delete;
			FunctionWrapper& operator=(FunctionWrapper && other) noexcept = default;

			void operator()() const
			{
				mImpl->call();
			}
			
		};

		std::mutex mQueueMutex;
		std::condition_variable mQueueCondition;
		std::atomic_bool mDone;
		ThreadSafeQueue<FunctionWrapper> mPoolQueue;
		std::vector<std::unique_ptr<ThreadSafeQueue<FunctionWrapper>>> mLocalQueues;
		std::vector<std::thread> mThreads;

		static thread_local ThreadSafeQueue<FunctionWrapper>* LocalQueue;
		static thread_local size_t LocalIndex;

		ThreadPool();
				
		static ThreadPool& instance();

		void workerThread(size_t index);
		void runPendingTask();
		bool popFromLocal(FunctionWrapper& task);
		bool popFromPool(FunctionWrapper& task);
		bool steal(FunctionWrapper& task);

		template <typename R, typename... Args>
		struct ResultsApplicator final
		{
			ResultsApplicator(std::promise<R>& promise, const std::function<R(Args...)> &func, const std::tuple<Args...>& args):
				mPromise(promise),
				mFunc(func),
				mArgs(args)
			{}

			void operator()()
			{
				mPromise.set_value(std::apply(mFunc, mArgs));
			}
			
		private:
			std::promise<R>& mPromise;
			const std::function<R(Args...)>& mFunc;
			const std::tuple<Args...>& mArgs;
		};

		template <typename... Args>
		struct ResultsApplicator<void, Args...> final
		{
			ResultsApplicator(std::promise<void>& promise, const std::function<void(Args...)>& func, const std::tuple<Args...>& args) :
				mPromise(promise),
				mFunc(func),
				mArgs(args)
			{}

			void operator()()
			{
				std::apply(mFunc, mArgs);
				mPromise.set_value();
			}

		private:
			std::promise<void>& mPromise;
			const std::function<void(Args...)>& mFunc;
			const std::tuple<Args...>& mArgs;
		};
		
		template<typename R, typename... Args>
		std::future<R> run(const std::function<R(Args...)>& task, Args... args)
		{
			std::promise<R> taskPromise;
			auto taskFuture = taskPromise.get_future();
			auto execFunc = FunctionWrapper
			{
				[taskPromise = std::move(taskPromise), task, tup = std::make_tuple(std::move(args)...)] () mutable
				{
					try
					{						
						ResultsApplicator(taskPromise, task, tup)();
					}
					catch (...)
					{
						taskPromise.set_exception(std::current_exception());
					}
				},
				nullptr
			};
			std::unique_lock pushLock{ mQueueMutex };
			if (LocalQueue != nullptr)
			{
				LocalQueue->push(std::move(execFunc));
			}
			else
			{
				mPoolQueue.push(std::move(execFunc));
			}
			mQueueCondition.notify_one();
			return taskFuture;
		}

		template <typename... Args, typename R>
		static void applyResult(std::promise<R>& promise, std::function<R(Args...)> func, const std::tuple<Args...> &args)
		{			
			promise.set_value(std::apply(*func, args));
		}
	};
}

#endif // THREAD_POOL_H
