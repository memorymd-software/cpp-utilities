#ifndef THREAD_SAFE_QUEUE_H
#define THREAD_SAFE_QUEUE_H

#include <memory>
#include <mutex>
#include <queue>

namespace Neuro::Utilities
{
	template<typename T>
	class ThreadSafeQueue final
	{
	public:
		void push(T value)
		{
			auto valuePtr = std::make_shared<T>(std::move(value));
			std::lock_guard pushLock{ mQueueMutex };
			mQueue.push(valuePtr);
			mDataCondition.notify_one();
		}

		std::shared_ptr<T> wait_try_pop()
		{
			std::unique_lock popLock{ mQueueMutex };			
			if (mQueue.empty())
			{
				mDataCondition.wait(popLock);
				if (mQueue.empty())
				{
					return std::shared_ptr<T>();
				}
			}
			auto result = mQueue.front();
			mQueue.pop();
			return result;
		}

		void interrupt_wait()
		{
			std::lock_guard interruptLock{ mQueueMutex };
			mDataCondition.notify_all();
		}
		
		std::shared_ptr<T> try_pop()
		{
			std::lock_guard popLock{ mQueueMutex };
			if (mQueue.empty())
			{
				return std::shared_ptr<T>();
			}
			auto result = mQueue.front();
			mQueue.pop();
			return result;
		}

		bool try_pop(T& value)
		{
			std::lock_guard popLock{ mQueueMutex };
			if (mQueue.empty())
			{
				return false;
			}

			value = std::move(*mQueue.front());
			mQueue.pop();
			return true;
		}
		
		bool empty() const
		{
			std::lock_guard popLock{ mQueueMutex };
			return mQueue.empty();
		}

	private:
		mutable std::mutex mQueueMutex;
		std::queue<std::shared_ptr<T>> mQueue;
		std::condition_variable mDataCondition;
	};
}
#endif // THREAD_SAFE_QUEUE_H
