#ifndef HERTZ_H
#define HERTZ_H

#include <utility>

namespace Neuro::Utilities {

	template <typename RepresentationField>
	struct Hertz final {
		explicit constexpr Hertz() :mValue(RepresentationField{ 0 }) {}
		explicit constexpr Hertz(const RepresentationField& value) : mValue(value) {}
		explicit constexpr Hertz(RepresentationField&& value) : mValue(std::move(value)) {}

		constexpr const RepresentationField& value() const noexcept {
			return mValue;
		}

		constexpr RepresentationField kilo() const noexcept {
			return mValue / RepresentationField{ 1'000 };
		}

		constexpr RepresentationField mega() const noexcept {
			return mValue / RepresentationField{ 1'000'000 };
		}

		constexpr RepresentationField& value() noexcept {
			return mValue;
		}

		constexpr RepresentationField milli() const noexcept {
			return mValue * RepresentationField{ 1'000 };
		}

		constexpr RepresentationField micro() const noexcept {
			return mValue * RepresentationField{ 1'000'000 };
		}

		constexpr RepresentationField nano() const noexcept {
			return mValue * RepresentationField{ 1'000'000'000 };
		}

	private:
		RepresentationField mValue;
	};

}

#endif // HERZ_H
