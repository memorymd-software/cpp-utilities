#ifndef VOLT_H
#define VOLT_H

#include <utility>

namespace Neuro::Utilities {

	template <typename RepresentationField>
	struct Volt final {
		constexpr Volt() :mValue(RepresentationField{ 0 }) {}
		explicit constexpr Volt(const RepresentationField& value) : mValue(value) {}
		explicit constexpr Volt(RepresentationField&& value) : mValue(std::move(value)) {}

		constexpr const RepresentationField& value() const noexcept {
			return mValue;
		}

		constexpr RepresentationField& value() noexcept {
			return mValue;
		}

		constexpr RepresentationField milli() const noexcept {
			return mValue * RepresentationField{ 1'000 };
		}

		constexpr RepresentationField micro() const noexcept {
			return mValue * RepresentationField{ 1'000'000 };
		}

		constexpr RepresentationField nano() const noexcept {
			return mValue * RepresentationField{ 1'000'000'000 };
		}

	private:
		RepresentationField mValue;
	};

	template <typename RepresentationField>
	Volt<RepresentationField> operator+(const Volt<RepresentationField>& lhs, const Volt<RepresentationField>& rhs)
	{
		return Volt<RepresentationField>{ lhs.value() + rhs.value() };
	}

	template <typename RepresentationField>
	Volt<RepresentationField> operator-(const Volt<RepresentationField>& lhs, const Volt<RepresentationField>& rhs)
	{
		return Volt<RepresentationField>{ lhs.value() - rhs.value() };
	}

	template <typename RepresentationField>
	Volt<RepresentationField> operator*(const Volt<RepresentationField>& lhs, const RepresentationField& rhs)
	{
		return Volt<RepresentationField>{ lhs.value()* rhs };
	}

	template <typename RepresentationField>
	Volt<RepresentationField> operator*(const RepresentationField& lhs, const Volt<RepresentationField>& rhs)
	{
		return rhs * lhs;
	}

	template <typename RepresentationField>
	Volt<RepresentationField> operator/(const Volt<RepresentationField>& lhs, const RepresentationField& rhs)
	{
		return Volt<RepresentationField>{ lhs.value() / rhs };
	}

}

#endif // VOLT_H
