#ifndef LOOP_H
#define LOOP_H

#include <atomic>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <thread>

namespace Neuro::Utilities
{
	template <typename>
	class Loop;

	template <typename R, typename... Args>
	class Loop<R(Args ...)>
	{
	public:
		using delay_time_t = std::chrono::duration<double>;

		template <typename Callable>
		Loop(Callable callable, delay_time_t delay, Args ... args):
			mCallable(callable),
			mDelay(delay)
		{
			mLoopThread = std::thread([=]()
			{
				while (mThreadRunning.load())
				{
					auto startTime = std::chrono::high_resolution_clock::now();
					try { mCallable(args...); }
					catch (...) { }
					auto stopTime = std::chrono::high_resolution_clock::now();
					try
					{
						auto execTime = std::chrono::duration_cast<delay_time_t>(stopTime - startTime);
						if (execTime < delay)
						{
							std::unique_lock<std::mutex> waitLock(mWaitMutex);
							mWaitCondition.wait_for(waitLock, delay - execTime, [=] { return !mThreadRunning.load(); });
						}
					}
					catch (...) { }
				}
			});
		}

		Loop(const Loop&) = delete;
		Loop& operator=(const Loop&) = delete;
		Loop(Loop&&) = delete;
		Loop& operator=(Loop&&) = delete;

		~Loop()
		{
			mThreadRunning.store(false);
			std::unique_lock<std::mutex> waitLock(mWaitMutex);
			mWaitCondition.notify_all();
			waitLock.unlock();
			try
			{
				if (mLoopThread.joinable())
					mLoopThread.join();
			}
			catch (std::system_error& e) { }
		}

		void setDelay(delay_time_t delay) { mDelay = delay; }

	private:
		static constexpr const char* class_name = "Loop";
		std::function<R(Args ...)> mCallable;
		delay_time_t mDelay;
		std::atomic<bool> mThreadRunning{true};
		std::thread mLoopThread;
		std::mutex mWaitMutex;
		std::condition_variable mWaitCondition;
	};
}

#endif // LOOP_H
