#ifndef PERCENT_H
#define PERCENT_H

#include <utility>

namespace Neuro::Utilities {

	template <typename RepresentationRing>
	struct Percent final {
		explicit constexpr Percent() :mValue(RepresentationRing{ 0 }) {}
		explicit constexpr Percent(const RepresentationRing& value) : mValue(value) {}
		explicit constexpr Percent(RepresentationRing&& value) : mValue(std::move(value)) {}

		constexpr const RepresentationRing& value() const noexcept {
			return mValue;
		}

		constexpr RepresentationRing& value() noexcept {
			return mValue;
		}

	private:
		RepresentationRing mValue;
	};

}

#endif // PERCENT_H
