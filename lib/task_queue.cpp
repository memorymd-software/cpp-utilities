#include "task_queue.h"
#include <atomic>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <string>

namespace Neuro::Utilities {

struct TaskQueue::Impl final {
	static constexpr const char *class_name = "TaskQueue";
	const std::string mName;
	std::queue<std::function<void()>> mExecutionQueue;
	std::atomic<bool> mIsRunning{ true };
	std::mutex mQueueMutex;
	std::condition_variable mQueueCondition;
	std::thread mExecThread;

	explicit Impl(const std::string &name):
		mName(name), 
		mExecThread([=]() { execFunction(); }) {
		if(!mExecThread.joinable()) {
			throw std::runtime_error("Task execution thread is not joinable");
		}
	}

	~Impl() {
		mIsRunning.store(false);
		std::unique_lock<std::mutex> queueLock(mQueueMutex);
		mQueueCondition.notify_all();
		queueLock.unlock();
		try {
			if (mExecThread.joinable())
				mExecThread.join();
		}
		catch (std::system_error &e) {
			throw;
		}
	}

	void execFunction() {
		while (mIsRunning.load()) {
			std::unique_lock<std::mutex> queueLock(mQueueMutex);
			while (mExecutionQueue.empty()) {
				if (!mIsRunning.load()) {
					return;
				}
				mQueueCondition.wait(queueLock);
			}
			auto task = mExecutionQueue.front();
			mExecutionQueue.pop();
			queueLock.unlock();

			try {
				task();
			}
			catch (std::exception &e) {
				throw;
			}
			catch (...) {
				throw;
			}
		}
	}
};

TaskQueue::TaskQueue(const std::string &name):
	mImpl(std::make_unique<Impl>(name)){
}

TaskQueue::TaskQueue(TaskQueue &&) noexcept = default;

TaskQueue& TaskQueue::operator=(TaskQueue &&) noexcept = default;

TaskQueue::~TaskQueue() = default;

void TaskQueue::exec(const std::function<void()> &task_function){
    std::unique_lock<std::mutex> queueLock(mImpl->mQueueMutex);
	mImpl->mExecutionQueue.push(task_function);
	mImpl->mQueueCondition.notify_one();
}

}
